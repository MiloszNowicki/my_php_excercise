<?php

namespace AppBundle\Controller;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use AppBundle\Entity\Brand;
use AppBundle\Entity\Model;


class DefaultController extends Controller
{



    /**
     *Matches /showmodel/*
     *
     * @Route("/showmodel/{id}")
     */
    public function showModel()
    {
        $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
        $qb->select('m, n')
            ->from(Model::class, 'm')
            ->join(
                'm.notes',
                'n'
            )
            ->orderBy('m.id', 'DESC');

        $ar = $qb->getQuery()->getResult();
        return $ar;

    }

}
